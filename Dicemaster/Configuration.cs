using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace Dicemaster
{
    public static class Configuration
    {
        public const string FilePath = "Data/config.json";
        private static Dictionary<ulong, ServerConfig> Configs;

        static Configuration()
        {
            // If the config exists, create it and write it to disk.
            if (File.Exists("Data/config.json")) {
                using (var stream = new StreamReader("Data/config.json")) {
                    var json = stream.ReadToEnd();
                    Configs = JsonConvert.DeserializeObject<Dictionary<ulong, ServerConfig>>(json);
                }

                return;
            }
            
            // If that file doesn't exist (such as if this is a fresh install), make the config empty and write that
            // to disk.
            
            Configs = new Dictionary<ulong, ServerConfig>();
            var blankJson = JsonConvert.SerializeObject(Configs);
            File.WriteAllText("Data/config.json", blankJson);
        }

        /// <summary>
        /// Get the config for a server.
        /// </summary>
        /// <param name="serverId">The server to get the config from.</param>
        /// <returns>The server config.</returns>
        public static ServerConfig GetConfig(ulong serverId)
        {
            // If no config exists, create the default one and write to disk
            if (!Configs.ContainsKey(serverId)) {
                Configs.Add(serverId, ServerConfig.Default);
            }

            return Configs[serverId];
        }

        /// <summary>
        /// Sets the config for a server.
        /// </summary>
        /// <param name="serverId">The server to set the config for.</param>
        /// <param name="serverConfig">The server config.</param>
        public static void SetConfig(ulong serverId, ServerConfig serverConfig)
        {
            // If there's no config for this server already, add to the dictionary.
            // Otherwise, replace the existing config with the new one.
            if (!Configs.ContainsKey(serverId)) {
                Configs.Add(serverId, serverConfig);
            }
            else {
                Configs[serverId] = serverConfig;
            }

            // Write to disk.
            var json = JsonConvert.SerializeObject(Configs);
            File.WriteAllText(FilePath, json);
        }
    }
}