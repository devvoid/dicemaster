﻿namespace Dicemaster
{
    internal class Program
    {
        private static void Main()
        {
            // Create an instance of the bot
            var dicemaster = new Dicemaster();
            
            // Start it up and wait for it to return (which it never will)
            dicemaster.Start().GetAwaiter().GetResult();
        }
    }
}