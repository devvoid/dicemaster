namespace Dicemaster
{
    public struct ServerConfig
    {
        /// <summary>
        /// The default data to use for the $roll,
        /// </summary>
        public (uint, uint, int) RollData;

        /// <summary>
        /// The default number of dice to use for $fudge,
        /// </summary>
        public int FudgeDice;

        /// <summary>
        /// The prefix to use for commands,
        /// </summary>
        public char CharPrefix;

        /// <summary>
        /// Create a new ServerConfig.
        /// </summary>
        /// <param name="rollData">The new default for $roll.</param>
        /// <param name="fudgeDice">The new default for $fudge.</param>
        /// <param name="charPrefix">The new default char prefix.</param>
        public ServerConfig((uint, uint, int) rollData, int fudgeDice, char charPrefix)
        {
            RollData = rollData;
            FudgeDice = fudgeDice;
            CharPrefix = charPrefix;
        }
        
        /// <summary>
        /// Gets the default server configuration.
        /// </summary>
        public static ServerConfig Default => new ServerConfig((1, 20, 0), 4, '$');
    }
}