using System;
using System.Text;
using System.Threading.Tasks;
using Discord.Commands;
using Discord.WebSocket;

namespace Dicemaster
{
    public class ConfigurationCommands : ModuleBase<SocketCommandContext>
    {
        [Command("setrolldefault")]
        [Summary("Sets the default string for rolling polyhedral dice. Default is 1d20+0.")]
        public async Task SetRollDefault([Remainder] string rollcmd)
        {
            var config = Configuration.GetConfig(Context.Guild.Id);
            config.RollData = Roll.ParseArgs(rollcmd);
            Configuration.SetConfig(Context.Guild.Id, config);
            
            await ReplyAsync("Roll default changed");
        }
        
        [Command("setfudgedefault")]
        [Summary("Sets the default number of fudge dice to roll. Default is 4.")]
        public async Task SetFudgeDefault([Remainder] int rollnumber)
        {
            var config = Configuration.GetConfig(Context.Guild.Id);
            config.FudgeDice = rollnumber;
            Configuration.SetConfig(Context.Guild.Id, config);
            
            await ReplyAsync("Fudge default changed");
        }

        [Command("setprefix")]
        [Summary("Sets the character prefix needed to run commands.")]
        public async Task SetCharPrefix(char newPrefix)
        {
            var config = Configuration.GetConfig(Context.Guild.Id);
            config.CharPrefix = newPrefix;
            Configuration.SetConfig(Context.Guild.Id, config);

            await ReplyAsync("Char prefix changed");
        }

        public static async Task DisplayConfig(SocketMessage message)
        {
            if (message.Content != "DisplayDicemasterConfiguration")
                return;
            
            if (!(message.Channel is SocketGuildChannel channel))
                return;

            var config = Configuration.GetConfig(channel.Guild.Id);
            
            var separator = config.RollData.Item3 < 0 ? '-' : '+';

            var sb = new StringBuilder();
            sb.AppendLine("```");
            sb.AppendLine("Configuration for this server:");
            sb.AppendLine($"\tCharacter prefix: {config.CharPrefix}");
            sb.Append($"\tRoll default: {config.RollData.Item1}d{config.RollData.Item2}");
            sb.AppendLine($"{separator}{Math.Abs(config.RollData.Item3)}");
            sb.AppendLine($"\tFudge default: {config.FudgeDice}");
            sb.AppendLine("```");

            await message.Channel.SendMessageAsync(sb.ToString());
        }
    }
}