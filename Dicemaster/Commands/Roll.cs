using System;
using System.Text;
using System.Threading.Tasks;
using Discord.Commands;
using Mayushii;

namespace Dicemaster
{
    public class Roll : ModuleBase<SocketCommandContext>
    {
        [Command("roll")]
        [Alias("r")]
        [Summary("Rolls polyhedral dice.")]
        public async Task RollD20([Remainder] string rollcmd = null)
        {
            var (numberOfDice, numberOfFaces, modifier) = rollcmd == null
                ? Configuration.GetConfig(Context.Guild.Id).RollData
                : ParseArgs(rollcmd);
            
            
            // Start building the message to send
            var sb = new StringBuilder();
            sb.Append($"{Context.User.Mention}: `");

            // For every dice, generate random number and add to the string builder
            var total = 0;
            for (var i = 0; i < numberOfDice; i++) {
                var result = RNG.Next(1, (int)numberOfFaces);
                total += result;
                
                sb.Append($"{result}");

                // If it's not the final die, add the separator
                if (i != numberOfDice - 1) {
                    sb.Append(" + ");
                }
            }

            if (modifier != 0) {
                // Add modifier to the total, and add to the text
                var modifierOperator = modifier > -1 ? '+' : '-';
                sb.Append($" ({modifierOperator}{Math.Abs(modifier)})");
                
                total += modifier;
            }
            
            // Finish the string and then reply.
            sb.Append($"`. Total: {total}");
            await ReplyAsync(sb.ToString());
        }

        public static (uint, uint, int) ParseArgs(string text)
        {
            // Item1 is the number of dice,
            // Item2 is the number of faces,
            // and Item3 is the modifier.
            (uint, uint, int) Data = (1, 20, 0);
            
            var split = text.Replace(" ", "").Split('d');

            // Only one arg, use that as the number of faces
            if (split.Length == 1) {
                Data.Item2 = uint.Parse(split[0]);
            }
            // Two or three args
            else {
                // Parse first arg
                Data.Item1 = uint.Parse(split[0]);
                
                // Find the first +
                var index = split[1].IndexOf('+');

                // If a + isn't found, search for - instead
                if (index == -1) {
                    index = split[1].IndexOf('-');
                }

                // If neither was found, then there are only two args
                if (index == -1) {
                    Data.Item2 = uint.Parse(split[1]);
                }
                else {
                    // If either one was found, then there are three args
                    Data.Item2 = uint.Parse(split[1].Substring(0, index));
                    Data.Item3 = int.Parse(split[1].Substring(index));
                }
            }

            return Data;
        }

        [Command("fudge")]
        [Alias("f")]
        [Summary("Rolls fudge dice")]
        public async Task RollFudge([Remainder] string rollcmd = null)
        {
            // Get number of dice
            var numberOfDice = rollcmd == null ? Configuration.GetConfig(Context.Guild.Id).FudgeDice
                : int.Parse(rollcmd.Replace(" ", ""));
            
            
            var sb = new StringBuilder();
            sb.Append($"{Context.User.Mention}: `");
            
            // For every dice, generate random number and add to the string builder
            var total = 0;
            for (var i = 0; i < numberOfDice; i++) {
                // Generate a number between 1 and 3 and subtract 2 to remap to (-1, 0, 1), for fudge dice
                var result = RNG.Next(1, 3) - 2;
                total += result;

                // Add to string builder
                sb.Append($"{Math.Abs(result)}");
                
                // If it's not the last die, add the proper operator to the builder
                if (i != numberOfDice - 1) {
                    sb.Append(result < 0 ? " - " : " + ");
                }
            }

            // Finish string and then reply
            sb.Append($"`. Total: {total}");
            await ReplyAsync(sb.ToString());
        }
    }
}