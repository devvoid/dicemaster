using System;

namespace Mayushii
{
    public static class RNG
    {
        private static readonly Random Random;

        static RNG()
        {
            Random = new Random();
        }

        /// <summary>
        /// Get the next random integer.
        /// </summary>
        /// <param name="min">The lowest number allowed.</param>
        /// <param name="max">The highest number allowed.</param>
        /// <returns></returns>
        public static int Next(int min, int max)
        {
            // It seems as though C# never returns the highest possible number,
            // so it's incremented by one to avoid that.
            return Random.Next(min, max + 1);
        }
    }
}
