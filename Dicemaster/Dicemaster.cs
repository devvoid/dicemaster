using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;

namespace Dicemaster
{
    public class Dicemaster
    {
        public static DiscordSocketClient Client;
        public static CommandService Commands;
        private string Token;
        
        public Dicemaster()
        {
            // Initialize all Discord-related stuff
            Client = new DiscordSocketClient();
            Commands = new CommandService();

            // Read the token
            Token = File.ReadAllText("Token.txt").Trim();
        }

        public async Task Start()
        {
            // Get all commands in this assembly
            await Commands.AddModulesAsync(Assembly.GetEntryAssembly(), null);

            // Add command handler to the MessageReceived event
            Client.MessageReceived += HandleCommandAsync;
            Client.MessageReceived += ConfigurationCommands.DisplayConfig;
            
            // Login
            await Client.LoginAsync(TokenType.Bot, Token);
            await Client.StartAsync();
            
            // Signal to the user that the bot's ready to go
            Console.WriteLine("Connection established.");
            
            // Wait forever
            await Task.Delay(-1);
        }
        
        private static async Task HandleCommandAsync(SocketMessage arg)
        {
            // If the message didn't come from a user, bail out, and convert to SocketUserMention at the same time
            if (!(arg is SocketUserMessage message))
                return;
            
            // Bots and webhooks can't trigger commands.
            if (message.Author.IsBot || message.Author.IsWebhook)
                return;

            // Can only roll in servers
            if (!(message.Channel is SocketGuildChannel channel))
                return;

            var prefix = Configuration.GetConfig(channel.Guild.Id).CharPrefix;

            // Check if it has the proper prefix
            var argPos = 0;
            if (!message.HasCharPrefix(prefix, ref argPos))
                return;
            
            // Create command context
            var context = new SocketCommandContext(Client, message);

            // Run command
            var result = await Commands.ExecuteAsync(context, argPos, null);
            
            // Check if it succeeded, send message if it wasn't
            if (!result.IsSuccess)
                await message.Channel.SendMessageAsync(result.ErrorReason);
        }
    }
}