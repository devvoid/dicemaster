﻿# Dicemaster General

A lightweight Discord bot that can roll both polyhedral and fudge dice.

## Rolling Dice

There are a few different ways of using the bot:

`$roll` - Rolls the default polyhedral dice.

`$roll <number of faces>` - Rolls one die.

`$roll <number of dice> d <number of faces>` - Rolls multiple dice.

`$roll <number of dice> d <number of faces> <+ or -> <modifier>` - Rolls multiple dice, with a modifier.

You can also use `$r` as a shorthand.

For fudge dice, use this command:

`$fudge` - Rolls the default number of fudge dice.

`$fudge <number of dice>` - Rolls multiple fudge dice.

You can also use `$f` as a shorthand.

In all rolling commands, spaces are ignored.

## Configuration

Dicemaster General can be configured to suit your own needs; the following commands will change the way the Dicemaster works by default.

`$setrolldefault <arguments>` - Sets the default polyhedral dice when using `$roll` without any arguments. Any of the argument styles from `$roll` will work here. Default is 1d20+0.

`$setfudgedefault <number of dice>` - Sets the default number of Fudge dice to roll when using `$fudge`. Default is 4.

`$setprefix <char>` - Sets the character prefix for using commands. Default is `$`. This should primarily be used to avoid conflicts with other bots.

## License

Licensed under either of

 * Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE](LICENSE) or http://opensource.org/licenses/MIT)

at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be dual licensed as above, without any
additional terms or conditions.
